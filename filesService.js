const fs = require('fs');
const path = require('path');
const filesFolder = path.resolve('./files');
const extensions = ['log','txt','json', 'yaml', 'xml', 'js'];

function createFile (req, res, next) {
  try{
    const { filename, content } = req.body;

    if (filename === undefined){
      res.status(400).json({ message: "Please add 'filename' parameter "});
      return;
    }
    if (content === undefined){
      res.status(400).json({ message: "Please add 'content' parameter "});
      return;
    }
    const fileExtension = path.extname(filename).split('.')[1];

    if(extensions.includes(fileExtension)){
      fs.writeFileSync(path.join(filesFolder,filename),content, {flag:'a+'});
      res.status(200).json({ message: "File was successfully created"});
    } else { 
      res.status(400).json({ message: "File doesn't have the appropriate extension" });
    }
  } catch (err){
    console.error(err.message);
    res.status(500).json({message : 'Server error'});
  }

}

function getFiles (req, res, next) {
  try{
    fs.readdir(filesFolder,(err,data) => {
      if(err){
        res.status(400).json({message: 'Invalid request'})
      } else {
        console.log()
        res.status(200).json({
          message: "Success",
          files: data
        });
      }
    })
  } catch (err){
    console.error(err.message);
    res.status(500).json({message : 'Server error'});
  }
}

const getFile = (req, res, next) => {
  try{
    fs.readFile(path.join(filesFolder,req.url),(err,data) => {
      if(err){
        res.status(400).json({message: 'Invalid request'})
      } else {
        res.status(200).json({
          message: "Success",
          filename: req.params.filename,
          content: data.toString(),
          extension: path.extname(req.params.filename).split('.')[1],
          uploadedDate: req._startTime
        });
      }
    })
    console.log(req);
  }
  catch (err){
    console.error(err.message);
    res.status(500).json({message : 'Server error'});
  }
}

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
}
